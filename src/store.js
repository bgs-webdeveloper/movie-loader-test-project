import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

let xhr = new XMLHttpRequest()

export default new Vuex.Store({
  state: {
    api_url_genres: 'http://api.themoviedb.org/3/genre/movie/list',
    api_url_movies: 'http://api.themoviedb.org/3/discover/movie', // Movie List Address
    api_url_movie: 'http://api.themoviedb.org/3/movie',
    api_Key: 'ebea8cfca72fdff8d2624ad7bbf78e4c',
    api_lang: 'en-US',

    page: 1,
    genre: null,
    movies: null,
    movie: null,
    movieVideo: null,

    selGenreId: null,
    selGenreName: null,

    selMovieId: null,

    moviePopup: false
  },
  mutations: {
    UPDATE_GENRE (state, res) {
      res = JSON.parse(res)
      state.genre = res.genres
      state.selGenreId = res.genres[0].id
      state.selGenreName = res.genres[0].name
    },
    UPDATE_MOVIES (state, res) {
      res = JSON.parse(res)
      state.total_pages = res.total_pages_movies
      state.movies = res.results
    },
    UPDATE_MOVIE (state, res) {
      res = JSON.parse(res)
      state.movie = res
    },
    UPDATE_MOVIE_VIDEO (state, res) {
      res = JSON.parse(res)
      state.movieVideo = res.results[0]
    }
  },
  actions: {
    getDataGenre (context) {
      let url = context.state.api_url_genres + '?api_key=' + context.state.api_Key + '&language=' + context.state.api_lang

      xhr.open('GET', url, false)
      xhr.send()

      if (xhr.status !== 200) {
        alert(xhr.status + ': ' + xhr.statusText)
      } else {
        context.commit('UPDATE_GENRE', xhr.responseText)
        context.dispatch('getDataMovies')
      }
    },
    getDataMovies (context) {
      let url = context.state.api_url_movies + '?api_key=' + context.state.api_Key + '&language=' + context.state.api_lang + '&sort_by=popularity.asc' + '&include_adult=false&include_video=true' + '&page=' + context.state.page + '&with_genres=' + context.state.selGenreId

      xhr.open('GET', url, false)
      xhr.send()

      if (xhr.status !== 200) {
        alert(xhr.status + ': ' + xhr.statusText)
      } else {
        context.commit('UPDATE_MOVIES', xhr.responseText)
      }
    },
    getDataMovie (context) {
      let url = context.state.api_url_movie + '/' + context.state.selMovieId + '?api_key=' + context.state.api_Key + '&language=' + context.state.api_lang

      xhr.open('GET', url, false)
      xhr.send()

      if (xhr.status !== 200) {
        alert(xhr.status + ': ' + xhr.statusText)
      } else {
        context.commit('UPDATE_MOVIE', xhr.responseText)
      }
    },
    getDataMovieVideo (context) {
      let url = context.state.api_url_movie + '/' + context.state.selMovieId + '/videos?api_key=' + context.state.api_Key + '&language=' + context.state.api_lang

      xhr.open('GET', url, false)
      xhr.send()

      if (xhr.status !== 200) {
        alert(xhr.status + ': ' + xhr.statusText)
      } else {
        context.commit('UPDATE_MOVIE_VIDEO', xhr.responseText)
      }
    }
  }
})
